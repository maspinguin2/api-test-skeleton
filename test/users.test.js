require('dotenv').config()
let chai = require('chai');
let expect = require('chai').expect;
let chaiHttp = require('chai-http');

chai.use(chaiHttp);

describe('API Testing', () => {

  it('it should failed to retrive all data because Unauthorized', (done) => {
    chai.request(process.env.SERVICEURL)
        .get('/admin/users')
        .end((err, res) => {
          expect(res.error).to.have.status(401);
          expect(res.body).to.have.property('message');
          expect(res.body.message).to.equal('Unauthorized');
          done();
        });
    });

    it('it should success to retrive users data', (done) => {
      chai.request('https://service-dev.tokocrypto.com')
          .get('/admin/users').set({'Authorization': process.env.ID_TOKEN,'Access-Token':process.env.ACCESS_TOKEN})
          .end((err, res) => {
              expect(res.body).to.have.status(200);
              expect(res.body.result).to.have.property('output');
              done();
          });
      });
});
